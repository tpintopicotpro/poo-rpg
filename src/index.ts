import Characters from "./Characters/Characters";
import Battle from "./battle"
import Werewolf from "./ennemy/Werewolf";
import Berserker from "./ennemy/Berserker"
import Golem from "./ennemy/Golem"
import Assassin from "./ennemy/Assassin"
import Griffin from "./ennemy/Griffin"
import BattleSimulation from "./battle-simulation";
import Dragon from "./ennemy/Dragon";
import { Hero } from "./Characters/hero";

const title = 'FIGHT' as string;

document.getElementById('content')!.innerHTML = `${title}`;

console.log(title, stringYoYo(title));


let hero = new Hero ("Ragondin", 500, 30, 1, 0, "Human");
let werewolf = new Werewolf ("Loup", 60, 20, 1, 0);

let berserker = new Berserker ("Garou", 80, 20, 1, 0); // attention attaque defaillante
let golem = new Golem ("Golemus", 100, 20, 1, 0)
let assassin = new Assassin ("Assassinus", 90, 20, 1, 0)
let griffin = new Griffin ("griffinus", 150, 20, 1, 0)
let dragon = new Dragon ("Dracofeu", 150, 20, 1, 0, "Volant")
//console.log("mon griffin", griffin.statement())
let arr = [werewolf, berserker, golem, assassin, griffin, dragon]

let combat = new Battle (); 
let combat2 = new BattleSimulation()
while(hero.getHealth() > 0){
for (let i = 0; i<arr.length ; i++){
    combat2.fight(hero, arr[i])
    if(hero.getXp()>=10){
        hero.level += 1
    }
    if (hero.getHealth()<0){
        console.log("Le joueur A est mort")
    }
}
}
// combat.fight(griffin, hero) 
// while(hero.getHealth() > 0){
//     combat2.fight(hero, griffin)
//     combat2.fight(hero, berserker)
//     combat2.fight(hero, dragon)
//     combat2.fight(hero, assassin)
//     combat2.fight(hero, werewolf)
//     combat2.fight(hero, golem)
//     // arr.forEach(element => {
//     //     combat2.fight(hero,element)
//     // });
// }

/**
 * Convertit une lettre sur 2 en majuscule
 * @param title 
 */
function stringYoYo(title: string): string
{
    const arr = title.split('');

    const newArr = arr.map((letter, index) => {
        if (index % 2 == 0) 
        {
            return letter.toUpperCase();
        }
        
        return letter;
        
    });

    return newArr.join('');
}