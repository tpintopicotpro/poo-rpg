import Dragon from "../ennemy/Dragon";
import Griffin from "../ennemy/Griffin";
import Characters from "./Characters";
import battle from "../battle"
import battleSimulation from "../battle-simulation"
// import { Thero } from "./Tclass";

export class Hero extends Characters {
    race: string
    constructor (name : string , health: number,hitStrength: number , level: number, xp: number, race: string) {
        super (name, health,hitStrength, level, xp)
            this.race = race
        }
    
    getName(name: string) {
        return this.name
    }
    setName (name: string) {
        this.name = name;
    }
    getHealth() {
        return this.health;
    }
    setHealth(damage: number) {
        if(this.race=="Dwarf"){
            let Rand = Math.floor(Math.random()* 5)
            console.log(Rand)
            if (Rand === 0){
                return this.health -= damage * 0.50
            }
            else {
                return this.health -= damage
            }   
        }
        else {
            return this.health -= damage
        }
    }
    attack(b: Characters) {
        // console.log(this.hitStrength, this.level)ennemy2
        if(this.race == "Elf"){
            
            if(b instanceof Dragon || b instanceof Griffin) {
                return this.hitStrength * 1.10
                
            }
            else {
                return this.hitStrength * 0.90
            }
        }
        if(this.race=="Human"){
            if(b instanceof Dragon || b instanceof Griffin){

                return this.hitStrength * 0.90
            }
            else {
                return this.hitStrength * 1.10
            }
        }

        return this.hitStrength * this.level;
    }
    die () {
        return console.log("Cette créature est morte")
    }
    getLvl () {
        return this.level
    }
    setLvl () {
        if (this.xp>=9){
            return this.level += 1
        }
    }
    getXp () {
        return this.xp
    }
    setXp () {
        this.xp += 2;
        return
    }
    getHitStrength () {
        return this.hitStrength;
    }
    setHitStrength () {
        return this.hitStrength
    }
    getRace(){
        return this.setRace();
    }
    setRace(){
        // if(this.race == "Elf"){
        //     if(Dragon || Griffin){
        //         return this.hitStrength * 1.10
        //     }
        //     else {
        //         return this.hitStrength * 0.90
        //     }
        // }
        // if(this.race=="Human"){
        //     if(Dragon || Griffin){
        //         return this.hitStrength * 0.90
        //     }
        //     else {
        //         return this.hitStrength * 1.10
        //     }
        // }

    }

    // getRace(){
    //     return this.setRace();
    // }
    // setRace(){
    //     return this.race
    // }
}
   
