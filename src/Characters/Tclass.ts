import Golem from "../ennemy/Golem"
import {Hero} from "./hero"
// export type Thero{
//     name : string;
//     race : string;
// }

export type Ttype = {
    type : "Volant" | "Sol"
}

// export enum Thero {
//     race {
//         "Humain" , "Dwarf" , "Elf"
//     } 
// }

export type Trace = {
    race : "Elf" | "Dwarf" | "Human"
}