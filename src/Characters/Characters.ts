import { Ttype } from "./Tclass";

export default class Characters {
    level = 1;
    name: string;
    health: number ;
    hitStrength : number;
    xp : number;
    // race : string;
    
    
    constructor(name: string , health: number , hitStrength: number, level: number, xp: number) {
        this.name = name;
        this.health = health;
        this.hitStrength = hitStrength;
        this.level = level;
        this.xp = xp;
        // this.race = race
        // this.type = type;
    }
    
    getName(name: string) {
        return this.name
    }
    setName (name: string) {
        this.name = name;
    }
    getHealth() {
        return this.health;
    }
    setHealth(damage: number) {
        this.health -= damage;
    }
    attack(b?: Characters) {
        // console.log(this.hitStrength, this.level)ennemy2
        return this.hitStrength * this.level;
    }
    die () {
        return console.log("Cette créature est morte")
    }
    getLvl () {
        return this.level
    }
    setLvl () {
        if (this.xp==10){
            this.level = this.level++
        }
    }
    getXp () {
        return this.xp
    }
    setXp () {
        this.xp += 2;
        return
    }
    getHitStrength () {
        return this.hitStrength;
    }
    setHitStrength () {
        return this.hitStrength
    }
    // getRace(){
    //     return this.setRace();
    // }
    // setRace(){
    //     return this.race
    // }
}